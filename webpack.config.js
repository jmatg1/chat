const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')


module.exports = {
    context: path.join(__dirname, 'src'),

    entry: './index.js',

    output: {
        filename: 'bundle.js',
        path: path.join(__dirname, 'dist'),
        publicPath:''
    },

    devtool: 'eval', // добавить исходные карты

    devServer: {
        port: 4200,
        open: true,
        publicPath: '/',
    },

    plugins: [
        new HtmlWebpackPlugin ({// подключит bundle, обновляет стрницу сам
            template: './index.html'
        }),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      }),
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
          {
            test: /\.scss$/,
            use: [{
              loader: "style-loader", options: {
                sourceMap: true
              }
            }, {
              loader: "css-loader", options: {
                sourceMap: true
              }
            }, {
              loader: "sass-loader", options: {
                sourceMap: true
              }
            }]
          },
          {
            test: /\.html$/,
            loader: 'html-loader'
            // loader: 'mustache-loader?minify'
            // loader: 'mustache-loader?{ minify: { removeComments: false } }'
            // loader: 'mustache-loader?noShortcut'
          }
        ]
    }

}
