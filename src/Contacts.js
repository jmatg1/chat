import { DataBase } from './DataBase'

export class Contacts extends DataBase {
  constructor () {
    super()
    this.sendId = null
    this.profile = null
    this.isRoom = false
  }

  /**
   * Возвращает пользователей, кроме залогиненного
   * @returns {[Object]}
   */
  getContacts () {
    const contacts = this.getItem('contacts')
    const profile = this.profile

    return contacts.filter(contact => contact.userId !== profile.userId)
  }

  /**
   * Получить массив объектов комнат
   * @return {[Object]}
   */
  getRooms () {
    const rooms = this.getItem('rooms')

    return rooms.filter(room => {
      if (room.createId === this.profile.userId) {
        room.isEdit = true
      }
      return room.usersId.some(id => id === this.profile.userId)
    })
  }

  /**
   * Получить комнату
   * @param id {Number} - ид комнаты
   * @return {Array}
   */
  getRoom (id) {
    const rooms = this.getItem('rooms')
    return rooms.filter(room => room.roomId === id)[0]
  }

  /**
   * Добавить комнату
   * @param name {String} - имя комнаты
   */
  addRoom (name) {
    const rooms = this.getItem('rooms')

    const newRoom = {
      'roomId': rooms.length + 1,
      'createId': this.profile.userId,
      'name': name,
      'usersId': [this.profile.userId]
    }

    rooms.push(newRoom)

    this.setItem('rooms', rooms)
  }

  /**
   * Удалить комнату
   * @param id {Number}
   */
  removeRoom (id) {
    const rooms = this.getItem('rooms')
    let updateRooms

    updateRooms = rooms.map(room => {
      if (room.roomId === id) {
        room.usersId = room.usersId
          .filter(usId =>
            usId !== this.profile.userId)
      }
      return room
    }).filter(room => room.roomId === id ? room.createId !== this.profile.userId : room)

    this.setItem('rooms', updateRooms)
  }

  /**
   * Редактировать комнату
   * @param id {Number} - комнаты
   * @param name {String} - комнаты
   * @param usersId {Array} - список юзеров в комнате, включая создателя
   */
  editRoom (id, name, usersId) {
    const rooms = this.getItem('rooms')
    const updateRoom = rooms.filter(room => room.roomId === id)[0]
    updateRoom.usersId = usersId
    updateRoom.name = name
    rooms.map(room => room.roomId === id ? room = updateRoom : room)

    this.setItem('rooms', rooms)
  }
  /**
   * Получить ид собседеника
   * @returns {Number}
   */
  getSendId () {
    return this.sendId
  }

  /**
   * Установить ид собеседника
   * @param  id {Number}
   * @param isRoom {Boolean}
   */
  setSendId (id, isRoom = false) {
    this.sendId = id
    this.isRoom = isRoom
  }

  /**
   * Установить данные текущего пользователя
   * @param prof {Object}
   */
  setProfile (prof) {
    this.profile = prof
  }

  /**
   * Получить данные текущего пользователя
   * @return {Object}
   */
  getProfile () {
    return this.profile
  }

  /**
   * Возвращает массив сообщений
   * @param  isRoom {Boolean}
   * @returns messagesUser {[Object]}
   */
  getChat (isRoom = this.isRoom) {
    const messages = this.getItem('messages')
    const contacts = this.getContacts()
    const profileId = this.profile.userId
    const sendId = this.sendId

    if (isRoom) {
      const rooms = this.getRooms()
      let messagesArray = null

      rooms.map(room => {
        if (room.roomId === sendId) {
          messagesArray = messages.filter(message => {
            if (message.roomId === room.roomId) {
              message.userName = contacts.filter(contact => contact.userId === message.userId).map(r => r.name)
              return true
            }
          })
        }
      })

      return messagesArray
    }

    const messagesUser = messages.filter(message =>
      message.userId === sendId &&
      message.sendId === profileId ||
      message.sendId === sendId &&
      message.userId === profileId
    )
    return (messagesUser)
  }

  /**
   * Отправить сообщение
   * @param text {String}
   * @param isRoom {Boolean}
   */
  sendMessage (text, isRoom = this.isRoom) {
    const profileId = this.profile.userId
    const messagesStor = this.getItem('messages')

    const newMessage = {
      messageId: messagesStor.length + 1,
      sendId: isRoom ? null : this.sendId,
      userId: profileId,
      roomId: isRoom ? this.sendId : null,
      text: text,
      dateCreate: new Date().toJSON()
    }
    messagesStor.push(newMessage)

    this.setItem('messages', messagesStor)
  }

  /**
   * Редактирует сообщение, если пришла пустая строка, то удалаляет все сообщение
   * @param id {Number}
   * @param text {String}
   */
  editMessage (id, text) {
    const messagesStor = this.getItem('messages')
    let newMessages = messagesStor

    if (text === '') {
      newMessages = messagesStor.filter(message => message.messageId !== id)
    } else {
      for (let i in newMessages) {
        if (newMessages[i].messageId === id) {
          newMessages[i].text = text
        }
      }
    }

    this.setItem('messages', newMessages)
  }
}
