import 'bootstrap/dist/css/bootstrap-reboot.min.css'
import './css/reset.css'
import './css/styles.scss'

import $ from 'jquery'
import Mustache from 'mustache'
import Moment from 'moment'

import htmlFormAuth from './template/formAuth.html'
import htmlMessage from './template/chat/message.html'
import htmlContact from './template/chat/contact.html'
import htmlChatLayout from './template/chat/chatLayout.html'
import htmlRoom from './template/chat/room.html'

import { DataBase } from './DataBase'
import { Contacts } from './Contacts'

const $authPopup = $('#jq-auth')

const dataBase = new DataBase()
const contacts = new Contacts()

dataBase.init()

$authPopup.show()
renderHTMLAuth()

// Регистрация/авторизация пользователя
$(document).on('submit', 'form#jq-auth-form', function (ev) {
  ev.preventDefault()
  const $name = $(this).find('input[name="name"]').val()
  const $password = $(this).find('input[name="password"]').val()
  const $auth = $(this).find('[name="auth"]:checked').val()
  let newProfile = null

  if ($auth === 'login') {
    newProfile = dataBase.login($name, $password)
    if (!newProfile) return alert('Неправильный логин, пароль')
  } else {
    newProfile = dataBase.registration($name, $password)
    if (!newProfile) return alert('Логин занят')
  }

  contacts.setProfile(newProfile)
  renderContacts()
  $authPopup.hide()
  $('#jq-chat').show()
})

// Выводит имя залогинненого и список его контактов
function renderContacts () {
  const contactsList = contacts.getContacts()
  const roomsList = contacts.getRooms()
  const profile = contacts.getProfile()

  renderHTMLChat(profile.name, contactsList, roomsList)

  handlerClickContact()
  handlerEnterSendMessage()
  handlerClickAddRoom()
  handlerClickRemoveRoom()
  handlerClickEditRoom()
  handlerClickSaveRoom()
}

// Обработка кликов по контакту и комнате
function handlerClickContact () {
  const $contactItem = $('.jq-contact')
  const $roomItem = $('.jq-room')
  const $allItem = $contactItem.add($roomItem)

  $contactItem.on('click', function () {
    if ($(this).hasClass('selected')) return

    const $userId = Number($(this).attr('data-user-id'))
    $allItem.removeClass('selected')
    $(this).addClass('selected')
    contacts.setSendId($userId)
    renderMessages(false)
  })

  $roomItem.on('click', function () {
    if ($(this).hasClass('selected')) return

    const $roomId = Number($(this).attr('data-room-id'))
    $allItem.removeClass('selected')
    $(this).addClass('selected')
    contacts.setSendId($roomId, true)
    renderMessages(true)
  })
}

// Добавить комнату
function handlerClickAddRoom () {
  $('#jq-room-add').on('click', function () {
    const nameRoom = window.prompt('Имя комнаты')

    if (nameRoom === null || nameRoom === '') return

    contacts.addRoom(nameRoom)
    renderContacts()
  })
}

// Удалить комнату
function handlerClickRemoveRoom () {
  $('.jq-room-remove').on('click', function () {
    const $roomId = Number($(this).parents('.rooms__item').find('.jq-room').attr('data-room-id'))
    contacts.removeRoom($roomId)
    renderContacts()
  })
}

// Редактировать комнату
function handlerClickEditRoom () {
  $('.jq-room-edit').on('click', function () {
    $(this).hide()
    $(this).siblings('.jq-room-save').show()

    const $room = $(this).parents('.rooms__item').find('.jq-room')
    const $roomId = Number($room.attr('data-room-id'))
    const usersRoom = contacts.getRoom($roomId)

    $room.attr('contenteditable', true).addClass('edit')
    $('input[name="usersInRoom[]"]').show().each(function () {
      const $checkId = Number($(this).val())
      if (usersRoom.usersId.some(id => id === $checkId)) {
        $(this).prop('checked', true)
      } else {
        $(this).prop('checked', false)
      }
    })
  })
}

function handlerClickSaveRoom () {
  $('.jq-room-save').on('click', function () {
    $(this).hide()
    $(this).siblings('.jq-room-edit').show()

    const $room = $(this).parents('.rooms__item').find('.jq-room')
    const $roomId = Number($room.attr('data-room-id'))
    $('input[name="usersInRoom[]"]').hide()
    let $checked = []
    $('input[name="usersInRoom[]"]:checked').each(function () {
      $checked.push(Number($(this).val()))
    })

    $room.attr('contenteditable', false).removeClass('edit')
    const $nameRoom = $room.text().trim()

    $checked.push(contacts.getProfile().userId)

    contacts.editRoom($roomId, $nameRoom, $checked)
  })
}

// Рендер сообщений
function renderMessages () {
  const profileId = contacts.getProfile().userId
  const $messagesList = $('#jq-messages-list').html('')
  const messages = contacts.getChat()
  let dataMessages = { message: [] }

  messages.map(message => {
    let classes = ['b-messages__item', 'message']

    if (message.userId === profileId) {
      classes.push('b-messages__item--me')
    }

    dataMessages.message.push({
      classes: classes.join(' '),
      messageId: message.messageId,
      text: message.text,
      userId: message.userId,
      userName: message.userName ? message.userName : '',
      dateCreate: Moment(message.dateCreate).locale('ru').fromNow()
    })
  })

  const rendered = Mustache.render(htmlMessage, dataMessages)
  $messagesList.html(rendered)
}

// Вызов submit формы отправки сообщений по Enter
function handlerEnterSendMessage () {
  $('#textareaId').keypress(function (e) {
    if (e.which === 13 && !e.shiftKey) {
      $(this).closest('form').submit()
      e.preventDefault()
      return false
    }
  })
}

// Обработка submit формы отправки сообщений
// Вызов реднера сообщений
$(document).on('submit', '#jq-form-message', function (ev) {
  ev.preventDefault()

  const $text = $(this).find('textarea[name="text"]')

  if ($text.val().length === 0) return

  contacts.sendMessage($text.val())
  $text.val('')
  renderMessages()
})

// Редатирование сообщения по клику, если оно пустое - удаляет
$(document).on('click', '.message__text', function () {
  if (Number($(this).attr('data-user-id')) !== contacts.getProfile().userId) return

  $(this).addClass('message__text--edit').attr('contenteditable', true).focus()

  $(this).on('focusout', function () {
    const $messageId = Number($(this).attr('data-message-id'))
    const $text = $(this).html()

    $(this).unbind()
    $(this).removeClass('message__text--edit').attr('contenteditable', false)

    if ($text.length === 0) {
      $(this).parent('.message').remove()
    }

    contacts.editMessage($messageId, $text)
  })
})

// Вызов происходит в другом окне, когда изменится localStorage
$(window).bind('storage', function () {
  renderMessages()
  renderContacts()
})

/*
Рендер HTML по средством mustache
 */

// Форма авторизации
function renderHTMLAuth () {
  const rendered = Mustache.render(htmlFormAuth, { login: 'Войти', registration: 'Регистрация' })
  $authPopup.html(rendered)
}

/**
 * Рендер чата
 * @param name {string} - имя пользователя
 * @param contacts {object} - объект контактов
 * @param rooms {object} - объект контактов
 */
function renderHTMLChat (name, contacts, rooms) {
  const chat = Mustache.render(htmlChatLayout,
    { name: name, contact: contacts, room: rooms },
    { contact: htmlContact, room: htmlRoom })

  $('#jq-chat').html(chat)
}
