export class DataBase {
  /**
   * Получить из localStorage
   * @param item
   * @returns {Object}
   */
  getItem (item) {
    return JSON.parse(window.localStorage.getItem(item))
  }

  /**
   * Записать в localStorage
   * @param key
   * @param value
   */
  setItem (key, value) {
    window.localStorage.setItem(key, JSON.stringify(value))
  }

  /**
   *  Проверяет сущетсвует ли данные в сторе, если нет, то заружает из файлов.
   */
  init () {
    const contactsJson = require('./data/contacts.json')
    const messagesJson = require('./data/messages.json')
    const roomsJson = require('./data/rooms.json')

    let contactsStor = this.getItem('contacts')
    let messagesStor = this.getItem('messages')
    let roomsStor = this.getItem('rooms')

    if (!contactsStor) {
      this.setItem('contacts', contactsJson)
    }

    if (!messagesStor) {
      this.setItem('messages', messagesJson)
    }

    if (!roomsStor) {
      this.setItem('rooms', roomsJson)
    }
  }

  /**
   * Вход в чат
   * @param name
   * @param password
   * @returns {boolean||object}
   */
  login (name, password) {
    const contacts = this.getItem('contacts')

    const res = contacts.find(contact => contact.name === name && contact.password === password)

    if (res) {
      return res
    }

    return false
  }

  /**
   * Регистрация нового пользователя
   * @param name
   * @param password
   * @returns {object}
   */
  registration (name, password) {
    const contacts = this.getItem('contacts')

    if (this.login(name, password)) {
      return
    }

    const newContact = {
      userId: contacts.length + 1,
      name,
      password,
      chatsId: []
    }

    contacts.push(newContact)
    this.setItem('contacts', contacts)

    return newContact
  }
}
